package com.example.gamelive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class GameLiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameLiveApplication.class, args);
    }

}
