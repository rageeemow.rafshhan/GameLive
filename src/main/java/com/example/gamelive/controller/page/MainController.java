package com.example.gamelive.controller.page;


import com.example.gamelive.converter.abstr.LevelMapper;
import com.example.gamelive.converter.abstr.TaskMapper;
import com.example.gamelive.converter.abstr.UserMapper;
import com.example.gamelive.model.entity.User;
import com.example.gamelive.service.abstr.LevelService;
import com.example.gamelive.service.abstr.TaskService;
import com.example.gamelive.service.abstr.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.security.Principal;

@Controller
@RequestMapping("/")
public class MainController {
    private final UsersService usersService;
    private final TaskService taskService;
    private final LevelService levelService;
    private final UserMapper userMapper;
    private final LevelMapper levelMapper;
    private final TaskMapper taskMapper;

    @Autowired
    public MainController(UsersService usersService,
                          TaskService taskService,
                          LevelService levelService,
                          UserMapper userMapper,
                          LevelMapper levelMapper,
                          TaskMapper taskMapper) {
        this.usersService = usersService;
        this.taskService = taskService;
        this.levelService = levelService;
        this.userMapper = userMapper;
        this.levelMapper = levelMapper;
        this.taskMapper = taskMapper;
    }

    @GetMapping("/profile")
    public String getProfile(Principal principal, ModelMap modelMap) {
        modelMap.addAttribute("username", principal.getName());
        return "profile";
    }
    @GetMapping("/admin")
    public String getAdmin() {
        return "admin";
    }
}
