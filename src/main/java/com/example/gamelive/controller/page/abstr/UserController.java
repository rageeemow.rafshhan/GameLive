package com.example.gamelive.controller.page.abstr;

import org.springframework.ui.ModelMap;

import java.security.Principal;

public interface UserController {
    String getProfile(Principal principal, ModelMap modelMap);
}
