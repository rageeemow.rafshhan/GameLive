package com.example.gamelive.controller.page.impl;

import com.example.gamelive.controller.page.abstr.AdminController;
import com.example.gamelive.service.abstr.UsersService;
import org.springframework.ui.ModelMap;

import java.security.Principal;


public class AdminControllerImpl  implements AdminController {
    private final UsersService usersService;

    public AdminControllerImpl(UsersService usersService) {
        this.usersService = usersService;
    }

    @Override
    public String getAdminPage(Principal principal, ModelMap modelMap) {
        return "admin";
    }
}
