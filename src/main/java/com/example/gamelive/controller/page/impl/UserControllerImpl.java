package com.example.gamelive.controller.page.impl;

import com.example.gamelive.controller.page.abstr.UserController;
import com.example.gamelive.service.abstr.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Map;

@Controller
@RequestMapping("/user/")
public class UserControllerImpl implements UserController {
    private final UsersService usersService;
    private final Map<String, Long> usernameId;

    @Autowired
    public UserControllerImpl(UsersService usersService, Map<String, Long> usernameId) {
        this.usersService = usersService;
        this.usernameId = usernameId;
    }


    @Override
    @GetMapping("/profile")
    public String getProfile(Principal principal, ModelMap modelMap) {
        if(!usernameId.containsKey(principal.getName())) {
            usernameId.put(
                    principal.getName(),
                    usersService.getUserIdByUsername(principal.getName())
            );
        }
        return "profile";
    }
}
