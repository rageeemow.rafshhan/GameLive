package com.example.gamelive.controller.rest;

import com.example.gamelive.model.entity.Task;
import com.example.gamelive.service.abstr.TaskService;
import com.example.gamelive.service.abstr.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.sql.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user/")
public class RestUserController {
    private final UsersService usersService;
    private final TaskService taskService;
    private final Map<String, Long> usernameId;

    @Autowired
    public RestUserController(UsersService usersService, TaskService taskService, Map<String, Long> usernameId) {
        this.usersService = usersService;
        this.taskService = taskService;
        this.usernameId = usernameId;
    }

    @GetMapping("/today'sTasks/")
    public ResponseEntity<List<Task>> getTasksToday(Principal principal) {
        if(!usernameId.containsKey(principal.getName())) {
            usernameId.put(
                    principal.getName(),
                    usersService.getUserIdByUsername(principal.getName())
            );
        }
        return new ResponseEntity<>(
                taskService.getTasksByUserIdAndStartPoint(
                        usernameId.get(principal.getName()),
                        new Date(System.currentTimeMillis())
                ),
                HttpStatus.FOUND
        );
    }
}
