package com.example.gamelive.converter.abstr;

import com.example.gamelive.model.dto.LevelDto;
import com.example.gamelive.model.entity.Level;

import javax.persistence.AttributeConverter;

public interface LevelMapper extends AttributeConverter<Level, LevelDto> {
}
