package com.example.gamelive.converter.abstr;

import com.example.gamelive.model.dto.TaskDto;
import com.example.gamelive.model.entity.Task;

import javax.persistence.AttributeConverter;

public interface TaskMapper extends AttributeConverter<Task, TaskDto> { }
