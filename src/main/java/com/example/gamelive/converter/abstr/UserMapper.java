package com.example.gamelive.converter.abstr;


import com.example.gamelive.model.dto.UserDto;
import com.example.gamelive.model.entity.User;
import javax.persistence.AttributeConverter;

public interface UserMapper extends AttributeConverter<User, UserDto> {
}
