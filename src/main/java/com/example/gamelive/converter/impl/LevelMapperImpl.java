package com.example.gamelive.converter.impl;

import com.example.gamelive.converter.abstr.LevelMapper;
import com.example.gamelive.model.dto.LevelDto;
import com.example.gamelive.model.entity.Level;
import com.example.gamelive.service.abstr.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Converter;

@Converter
@Component
public class LevelMapperImpl implements LevelMapper {

    private final LevelService levelService;

    @Autowired
    public LevelMapperImpl(LevelService levelService) {
        this.levelService = levelService;
    }

    @Override
    public LevelDto convertToDatabaseColumn(Level level) {
        return LevelDto.builder()
                .id(level.getId())
                .gettingCountBalls(level.getGettingCountBalls())
                .fullCountBallsOfThisLevel(level.getFullCountBallsOfThisLevel())
                .mainTaskId(level.getMainTask().getTaskId())
                .typeOfDuty(level.getTypeOfDuty().name())
                .userId(level.getUser().getUserId())
                .build();
    }

    @Override
    public Level convertToEntityAttribute(LevelDto levelDto) {
        return levelService.findById(levelDto.id());
    }
}
