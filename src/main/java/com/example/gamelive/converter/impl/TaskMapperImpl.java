package com.example.gamelive.converter.impl;

import com.example.gamelive.converter.abstr.TaskMapper;
import com.example.gamelive.model.dto.TaskDto;
import com.example.gamelive.model.entity.Task;
import com.example.gamelive.service.abstr.TaskService;
import com.example.gamelive.service.abstr.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Converter;

@Converter
@Component
public class TaskMapperImpl implements TaskMapper {

    private final TaskService taskService;
    private final UsersService usersService;

    @Autowired
    public TaskMapperImpl(TaskService taskService, UsersService usersService) {
        this.taskService = taskService;
        this.usersService = usersService;
    }

    @Override
    public TaskDto convertToDatabaseColumn(Task task) {
        return TaskDto
                .builder()
                .id(task.getTaskId())
                .descriptionTask(task.getDescriptionTask())
                .isPerformed(task.getIsPerformed())
                .leadTime(task.getLeadTime())
                .moneyCapital(task.getMoneyCapital())
                .startPoint(task.getStartPoint())
                .userId(task.getUser().getUserId())
                .countGetBalls(task.getCountGetBalls())
                .build();
    }

    @Override
    public Task convertToEntityAttribute(TaskDto taskDto) {
        return taskService.findById(taskDto.id());
    }
}
