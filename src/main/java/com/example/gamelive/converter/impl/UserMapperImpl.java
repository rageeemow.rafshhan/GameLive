package com.example.gamelive.converter.impl;

import java.util.List;
import javax.persistence.Converter;
import com.example.gamelive.model.dto.UserDto;
import com.example.gamelive.model.entity.User;
import com.example.gamelive.model.entity.Level;
import org.springframework.stereotype.Component;
import com.example.gamelive.model.entity.enums.Role;
import com.example.gamelive.service.abstr.LevelService;
import com.example.gamelive.service.abstr.UsersService;
import com.example.gamelive.converter.abstr.UserMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class UserMapperImpl implements UserMapper {
    UsersService usersService;
    LevelService levelService;

    @Value("USER")
    Role role;

    @Autowired
    public UserMapperImpl(UsersService usersService, LevelService levelService) {
        this.usersService = usersService;
        this.levelService = levelService;
    }

    @Override
    public UserDto convertToDatabaseColumn(User user) {
        return UserDto.builder()
                .id(user.getUserId())
                .role(user.getRole().toString())
                .username(user.getUsername())
                .description(user.getDescription())
                .build();
    }

    @Override
    public User convertToEntityAttribute(UserDto userDto) {
        return usersService.findById(userDto.id());
    }
}
