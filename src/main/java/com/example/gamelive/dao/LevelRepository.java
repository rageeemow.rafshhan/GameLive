package com.example.gamelive.dao;

import com.example.gamelive.model.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface LevelRepository extends JpaRepository<Level, Long> {
}