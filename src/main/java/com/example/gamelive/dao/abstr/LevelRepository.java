package com.example.gamelive.dao.abstr;

import com.example.gamelive.dao.abstr.base.EntityRepository;
import com.example.gamelive.model.entity.Level;

import java.util.List;

public interface LevelRepository extends EntityRepository<Level, Long> {
}
