package com.example.gamelive.dao.abstr;

import com.example.gamelive.dao.abstr.base.EntityRepository;
import com.example.gamelive.model.entity.Task;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface TaskRepository extends EntityRepository<Task, Long> {
    public List<Task> getTasksByUserIdAndStartPoint(Long userId, Date startPoint);
    void savePerformOfTask(@Param("task_id") Long taskId);
}
