package com.example.gamelive.dao.abstr;

import com.example.gamelive.dao.abstr.base.EntityRepository;
import com.example.gamelive.model.entity.User;

public interface UserRepository extends EntityRepository<User, Long> {
    User getUserByUsername(String username);
    void save(User user);
    Long getUserIdByUsername(String username);
    void deleteUserById(Long Id);
}
