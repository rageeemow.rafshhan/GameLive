package com.example.gamelive.dao.abstr.base;


public interface EntityRepository<E, ID> {
    void save(E entity);
    E update(E entity);
    E findById(ID id);
    void remove(E entity);
    Class<E> getEntityClass();
}
