package com.example.gamelive.dao.impl;

import com.example.gamelive.dao.abstr.LevelRepository;
import com.example.gamelive.dao.impl.base.EntityRepositoryImpl;
import com.example.gamelive.model.entity.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class LevelRepositoryImpl
        extends EntityRepositoryImpl<Level, Long>
        implements LevelRepository {

    private final EntityManager entityManager;

    @Autowired
    public LevelRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public void remove(Level entity) {
        Query query = entityManager.createQuery("delete from Level where id = :id");
        query.setParameter("id", entity.getId());
        query.executeUpdate();
    }

    @Override
    public Class<Level> getEntityClass() {
        return Level.class;
    }
}
