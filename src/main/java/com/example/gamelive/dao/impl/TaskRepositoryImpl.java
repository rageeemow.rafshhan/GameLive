package com.example.gamelive.dao.impl;

import com.example.gamelive.dao.abstr.TaskRepository;
import com.example.gamelive.dao.impl.base.EntityRepositoryImpl;
import com.example.gamelive.model.entity.Level;
import com.example.gamelive.model.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class TaskRepositoryImpl
        extends EntityRepositoryImpl<Task, Long>
        implements TaskRepository {
    private final EntityManager entityManager;

    @Autowired
    public TaskRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public void remove(Task entity) {
        Query query = entityManager.createQuery("delete from Task where taskId = :id");
        query.setParameter("id", entity.getId());
        query.executeUpdate();
    }

    @Override
    public Class<Task> getEntityClass() {
        return Task.class;
    }

    @Override
    public List<Task> getTasksByUserIdAndStartPoint(Long userId, Date startPoint) {
        TypedQuery<Task> query = entityManager.createQuery(
                "SELECT t " +
                        "from Task t " +
                        "where t.user.userId = :userId " +
                        "and t.startPoint = :startPoint",
                Task.class);
        query.setParameter("userId", userId);
        query.setParameter("startPoint", startPoint);
        return query.getResultList();
    }

    @Override
    public void savePerformOfTask(Long taskId) {
        TypedQuery<Task> query = entityManager.createQuery(
                "update Task " +
                        "set isPerformed = true, " +
                        "leadTime = :currentTimestamp " +
                        "where taskId = :taskId", Task.class);
        query.setParameter("taskId", taskId);
        query.setParameter("currentTimestamp", new Timestamp(System.currentTimeMillis()));
        query.executeUpdate();
    }
}
