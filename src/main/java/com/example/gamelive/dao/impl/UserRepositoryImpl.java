package com.example.gamelive.dao.impl;

import com.example.gamelive.dao.abstr.UserRepository;
import com.example.gamelive.dao.impl.base.EntityRepositoryImpl;
import com.example.gamelive.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


@Repository
public class UserRepositoryImpl
        extends EntityRepositoryImpl<User, Long>
        implements UserRepository {

    private final EntityManager entityManager;

    @Autowired
    public UserRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public User getUserByUsername(String username) {
        TypedQuery<User> query = entityManager.createQuery(
                "select u from User u where u.username = :username", User.class
        );
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    @Override
    public Long getUserIdByUsername(String username) {
        TypedQuery<Long> query  = entityManager.createQuery(
                "select u.userId from User u where u.username = :username", Long.class
        );
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    @Override
    public void deleteUserById(Long id) {
        Query query = entityManager.createQuery("delete from User where userId = :userId");
        query.setParameter("userId", id);
    }

    @Override
    public void remove(User entity) {
        Query query = entityManager.createQuery("delete from User where userId = :id");
        query.setParameter("id", entity.getId());
        query.executeUpdate();
    }

    @Override
    public  Class<User> getEntityClass() {
        return User.class;
    }

}
