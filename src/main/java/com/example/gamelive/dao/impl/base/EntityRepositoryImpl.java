package com.example.gamelive.dao.impl.base;

import com.example.gamelive.dao.abstr.base.EntityRepository;
import com.example.gamelive.model.entity.base.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public abstract class EntityRepositoryImpl<E extends BaseEntity, ID> implements EntityRepository<E, ID> {
    private final EntityManager entityManager;

    public EntityRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(E entity) {
        entityManager.persist(entity);
    }

    @Override
    public E update(E entity) {
        return entityManager.merge(entity);
    }

    @Override
    public E findById(ID id) {
        return entityManager.find(getEntityClass(), id);
    }

    public abstract Class<E> getEntityClass();
}
