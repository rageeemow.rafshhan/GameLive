package com.example.gamelive.model.dto;

import lombok.Builder;
import java.io.Serializable;

@Builder
public record LevelDto(
        Long id,
        Long gettingCountBalls,
        Long fullCountBallsOfThisLevel,
        Long userId,
        String typeOfDuty,
        Long mainTaskId
) implements Serializable {
}
