package com.example.gamelive.model.dto;

import lombok.Builder;
import java.io.Serializable;

@Builder
public record UserDto(
        Long id,
        String username,
        char[] password,
        String description,
        String role) implements Serializable {
}
