package com.example.gamelive.model.entity;


import javax.persistence.*;

@Entity
@Table
public class Employee {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    String name;

    @ManyToOne()
    @JoinColumn(name = "department_id")
    Department department;

    @ManyToOne
    @JoinColumn(name = "chief_id")
    Employee chief;

    @Column
    Long salary;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
