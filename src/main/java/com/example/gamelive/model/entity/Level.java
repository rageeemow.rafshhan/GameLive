package com.example.gamelive.model.entity;

import com.example.gamelive.model.entity.base.BaseEntity;
import com.example.gamelive.model.entity.enums.TypeOfDuty;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "level")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Level implements BaseEntity {
    @Id
    @Column(name = "level_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private Long numberLevel;

    @OneToOne
    @JoinColumn(name = "main_task_id")
    private Task mainTask;

    @Column
    private Long gettingCountBalls;

    @Column
    private Long fullCountBallsOfThisLevel;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private TypeOfDuty typeOfDuty;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Level level = (Level) o;
        return Objects.equals(id, level.id) && Objects.equals(numberLevel, level.numberLevel) && Objects.equals(gettingCountBalls, level.gettingCountBalls) && Objects.equals(fullCountBallsOfThisLevel, level.fullCountBallsOfThisLevel) && Objects.equals(user, level.user) && typeOfDuty == level.typeOfDuty;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numberLevel, gettingCountBalls, fullCountBallsOfThisLevel, user, typeOfDuty);
    }
}