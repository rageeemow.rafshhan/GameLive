package com.example.gamelive.model.entity;

import com.example.gamelive.model.entity.base.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "task")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task implements BaseEntity {
    @Id
    @Column(name = "task_id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long taskId;

    @Column(length = 150, nullable = false)
    private String name;

    @Column
    private String descriptionTask;

    @Column
    private Date startPoint;

    @Column
    private Boolean isPerformed;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private Long moneyCapital;

    @Column
    private long countGetBalls;

    @Column
    private Timestamp leadTime; /** <-- Точное время выполнения */
    @Override
    public Long getId() {
        return taskId;
    }
}