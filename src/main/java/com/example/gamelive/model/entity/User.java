package com.example.gamelive.model.entity;

import com.example.gamelive.model.entity.base.BaseEntity;
import com.example.gamelive.model.entity.enums.Role;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Entity
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User implements UserDetails, BaseEntity {

    public static PasswordEncoder passwordEncoder;
    @Id
    @Column(name = "user_id", nullable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long userId;


    @Column(nullable = false, unique = true, length = 30)
    private String username; /** or login */

    @Column(nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "friends",
            joinColumns = @JoinColumn(name = "friend1"),
            inverseJoinColumns = @JoinColumn(name = "friend2")
    )
    private List<User> friends;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Task> tasks;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Level> levels;

    @Column
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Stream
                .of(role)
                .toList();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username) &&
                Objects.equals(passwordEncoder.upgradeEncoding(password), passwordEncoder.upgradeEncoding(user.password)) &&
                Objects.equals(description, user.description)
                && role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, description, role);
    }

    @Override
    public Long getId() {
        return userId;
    }
}