package com.example.gamelive.model.entity.base;

public interface BaseEntity {
    Long getId();
}
