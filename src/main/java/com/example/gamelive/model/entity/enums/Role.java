package com.example.gamelive.model.entity.enums;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN, USER;

    @Override
    public String getAuthority() {
        return this.name();
    }

    @Override
    public String toString() {
        return this.name();
    }

     public Role findRoleByName(String name) {
        for (Role role : this.getDeclaringClass().getEnumConstants()) {
            if (name.equals(role.name())) {
                return role;
            }
        }
        return null;
    }
}
