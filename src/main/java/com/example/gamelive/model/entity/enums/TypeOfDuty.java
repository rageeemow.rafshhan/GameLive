package com.example.gamelive.model.entity.enums;

public enum TypeOfDuty {
    BODY, PERSONAL, JOB, ANOTHER;

    @Override
    public String toString() {
        return this.name();
    }
}
