package com.example.gamelive.service.abstr;

import com.example.gamelive.model.entity.Level;
import com.example.gamelive.service.abstr.base.EntityService;

import java.util.List;

public interface LevelService extends EntityService<Level, Long> {
}
