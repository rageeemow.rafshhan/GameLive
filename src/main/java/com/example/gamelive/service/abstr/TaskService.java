package com.example.gamelive.service.abstr;

import com.example.gamelive.model.entity.Task;
import com.example.gamelive.service.abstr.base.EntityService;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface TaskService extends EntityService<Task, Long> {
    List<Task> getTasksByUserIdAndStartPoint(Long userId, Date startPoint);
    void savePerformOfTask(@Param("task_id") Long taskId);
}
