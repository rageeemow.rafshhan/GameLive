package com.example.gamelive.service.abstr;

import com.example.gamelive.model.entity.User;
import com.example.gamelive.service.abstr.base.EntityService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UsersService extends UserDetailsService, EntityService<User, Long> {
    @Override
    User loadUserByUsername(String username) throws UsernameNotFoundException;
    Long getUserIdByUsername(String username);
    void deleteUserById(Long id);
}
