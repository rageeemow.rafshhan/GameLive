package com.example.gamelive.service.abstr.base;

public interface EntityService<E, ID> {
    void save(E entity);
    E update(E entity);
    E findById(ID id);
    void remove(E entity);
}
