package com.example.gamelive.service.impl;

import com.example.gamelive.dao.abstr.LevelRepository;
import com.example.gamelive.model.entity.Level;
import com.example.gamelive.service.abstr.LevelService;
import com.example.gamelive.service.impl.base.EntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LevelServiceImpl extends EntityServiceImpl<Level, Long> implements LevelService {
    private LevelRepository levelRepository;

    @Autowired
    public LevelServiceImpl(LevelRepository levelRepository) {
        super(levelRepository);
        this.levelRepository = levelRepository;
    }

}
