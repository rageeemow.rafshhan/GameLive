package com.example.gamelive.service.impl;


import com.example.gamelive.dao.abstr.TaskRepository;
import com.example.gamelive.model.entity.Task;
import com.example.gamelive.service.abstr.TaskService;
import com.example.gamelive.service.impl.base.EntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Service
public class TaskServiceImpl extends EntityServiceImpl<Task, Long> implements TaskService {
    private TaskRepository taskRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    @Transactional
    public List<Task> getTasksByUserIdAndStartPoint(Long userId, Date startPoint) {
        return taskRepository.getTasksByUserIdAndStartPoint(userId, startPoint);
    }

    @Override
    @Transactional
    public void savePerformOfTask(Long taskId) {

    }
}
