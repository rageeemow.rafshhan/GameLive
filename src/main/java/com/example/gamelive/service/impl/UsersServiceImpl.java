package com.example.gamelive.service.impl;


import com.example.gamelive.dao.abstr.UserRepository;
import com.example.gamelive.model.entity.User;
import com.example.gamelive.service.abstr.UsersService;
import com.example.gamelive.service.impl.base.EntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UsersServiceImpl extends EntityServiceImpl<User, Long> implements UsersService {
    private final UserRepository userRepository;


    @Autowired
    public UsersServiceImpl(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.getUserByUsername(username);
    }

    @Override
    @Transactional
    public Long getUserIdByUsername(String username) {
        return userRepository.getUserIdByUsername(username);
    }

    @Override
    @Transactional
    public void deleteUserById(Long id) {
        userRepository.deleteUserById(id);
    }

    @Override
    @Transactional
    public void save(User entity) {
        super.save(entity);
    }

    @Override
    @Transactional
    public User update(User entity) {
        return super.update(entity);
    }

    @Override
    @Transactional
    public User findById(Long aLong) {
        return super.findById(aLong);
    }

    @Override
    @Transactional
    public void remove(User entity) {
        super.remove(entity);
    }
}
