package com.example.gamelive.service.impl.base;


import com.example.gamelive.dao.abstr.base.EntityRepository;
import com.example.gamelive.service.abstr.base.EntityService;

import javax.transaction.Transactional;

public abstract class EntityServiceImpl<E, ID> implements EntityService<E, ID> {
    private final EntityRepository<E, ID> entityRepository;

    protected EntityServiceImpl(EntityRepository<E, ID> entityRepository) {
        this.entityRepository = entityRepository;
    }

    @Override
    @Transactional
    public void save(E entity) {
        entityRepository.save(entity);
    }

    @Override
    @Transactional
    public E update(E entity) {
        return entityRepository.update(entity);
    }

    @Override
    @Transactional
    public E findById(ID id) {
        return entityRepository.findById(id);
    }

    @Override
    @Transactional
    public void remove(E entity) {
        entityRepository.remove(entity);
    }

}
