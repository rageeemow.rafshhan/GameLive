package com.example.gamelive.service.impl;

import com.example.gamelive.model.entity.Level;
import com.example.gamelive.model.entity.User;
import com.example.gamelive.service.abstr.LevelService;
import com.example.gamelive.service.abstr.UsersService;
import com.example.gamelive.service.impl.base.EntityServiceTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import java.util.List;

@SpringBootTest
class LevelServiceTest implements EntityServiceTest<Level, Long> {

    private final LevelService levelService;

    private final UsersService usersService;

    private final EntityManager entityManager;
    private User user;

    private Level level;

    @Autowired
    LevelServiceTest(LevelService levelService, UsersService usersService, EntityManager entityManager) {
        this.levelService = levelService;
        this.usersService = usersService;
        this.entityManager = entityManager;
        this.user = usersService.loadUserByUsername("rageeemow.rafshhan@gmail.com");
        this.level = Level
                .builder()
                .numberLevel(1L)
                .fullCountBallsOfThisLevel(1000L)
                .user(user)
                .build();
    }


    @Test
    void allTests() {
        save();
        findById();
        update();
        remove();
    }

    @Override
    @Test
    public void save() {
        Assertions.assertDoesNotThrow(() -> {
            levelService.save(level);
        });
        level = getAllLevels().get(0);
    }

    @Override
    @Test
    public void update() {
        Assertions.assertDoesNotThrow(() -> {
            level.setNumberLevel(2L);
            levelService.update(level);
        });

    }

    @Override
    @Test
    public void findById() {
        Assertions.assertTrue(() -> {
            Level level2 = levelService.findById(level.getId());
            return level.equals(level2);
        });
    }

    @Override
    @Test
    public void remove() {
        Assertions.assertTrue(() -> {
            List<Level> allLevels = getAllLevels();
            level = allLevels.get(0);
            int length = allLevels.size();
            levelService.remove(level);
            return getAllLevels().size() == length - 1;
        });
    }

    List<Level> getAllLevels() {
        return entityManager.createQuery("select level from Level level", Level.class).getResultList();
    }
}