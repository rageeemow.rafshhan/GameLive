package com.example.gamelive.service.impl;

import com.example.gamelive.model.entity.Task;
import com.example.gamelive.model.entity.User;
import com.example.gamelive.service.abstr.TaskService;
import com.example.gamelive.service.abstr.UsersService;
import com.example.gamelive.service.impl.base.EntityServiceTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.List;

@SpringBootTest
class TaskServiceTest implements EntityServiceTest<Task, Long> {
    private final TaskService taskService;
    private final UsersService usersService;
    private final EntityManager entityManager;

    private final User testUser;
    private Task task;

    @Autowired
    TaskServiceTest(TaskService taskService, UsersService usersService, EntityManager entityManager) {
        this.taskService = taskService;
        this.usersService = usersService;
        this.testUser = usersService.loadUserByUsername("rageeemow.rafshhan@gmail.com");
        this.entityManager = entityManager;
        this.task = Task.builder()
                .name("nameTask")
                .descriptionTask("descriptionTask")
                .startPoint(new Date(System.currentTimeMillis()))
                .user(testUser)
                .build();

    }

    @Test
    void allTests() {
        save();
        getTasksByUserIdAndStartPoint();
        savePerformOfTask();
        update();
        findById();
        remove();
    }

    @Override
    @Test
    public void save() {
        Assertions.assertTrue(() -> {
            int length = getAllTask().size();
            taskService.save(task);
            return getAllTask().size() == length + 1;
        });
    }

    @Test
    void getTasksByUserIdAndStartPoint() {
        Assertions.assertTrue(() -> {
            List<Task> tasks = taskService.getTasksByUserIdAndStartPoint(testUser.getUserId(), new Date(System.currentTimeMillis()));
            task = tasks.get(0);
            return tasks.size() != 0;
        }
        );
    }

    @Test
    void savePerformOfTask() {
        Assertions.assertDoesNotThrow(() -> taskService.savePerformOfTask(task.getTaskId()));
    }

    @Override
    @Test
    public void update() {
        Assertions.assertDoesNotThrow(() -> {
            task.setName("testUpdatingNameOfTask");
            task = taskService.update(task);
        });
    }

    @Override
    @Test
    public void findById() {
        Assertions.assertDoesNotThrow(() -> {
            taskService.findById(task.getId());
        });
    }

    @Override
    @Test
    public void remove() {
        Assertions.assertTrue(() -> {
            int length = getAllTask().size();
            taskService.remove(task);
            return getAllTask().size() == length - 1;
        });
    }

    List<Task> getAllTask() {
        return entityManager.createQuery("select task From Task task", Task.class).getResultList();
    }
}