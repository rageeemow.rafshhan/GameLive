package com.example.gamelive.service.impl;

import com.example.gamelive.model.entity.User;
import com.example.gamelive.model.entity.enums.Role;
import com.example.gamelive.service.abstr.UsersService;
import com.example.gamelive.service.impl.base.EntityServiceTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityManager;
import java.util.List;

@SpringBootTest
class UsersServiceTest implements EntityServiceTest<User, Long> {
    private final UsersService usersService;
    private final PasswordEncoder passwordEncoder;
    private final EntityManager entityManager;
    private final String username = "test.username";
    private User testUser;

    private List<User> users;

    @Autowired
    UsersServiceTest(UsersService usersService, PasswordEncoder passwordEncoder, EntityManager entityManager) {
        this.usersService = usersService;
        this.passwordEncoder = passwordEncoder;
        testUser = User.builder()
                .role(Role.USER)
                .password(passwordEncoder.encode("test_password"))
                .username(username).build();
        this.entityManager = entityManager;
        users = getAllUsers();
    }

    @Test
    public void allTests() {
        save();
        loadUserByUsername();
        update();
        findById();
        remove();
    }

    @Override
    @Test
    public void save() {
        Assertions.assertTrue(
                () -> {
                    int length = users.size();
                    usersService.save(testUser);
                    return getAllUsers().size() == length + 1;
                }
        );

    }

    List<User> getAllUsers() {
        users = entityManager.createQuery("select u from User u",  User.class).getResultList();
        return users;
    }

    @Test
    void loadUserByUsername() throws UsernameNotFoundException {
        Assertions.assertTrue(() -> testUser.equals(usersService.loadUserByUsername(username)));
    }

    @Override
    @Test
    public void update() {
        testUser = usersService.loadUserByUsername(username);
        testUser.setPassword(passwordEncoder.encode("test_password2"));
        Assertions.assertDoesNotThrow(
                () -> usersService.update(testUser)
        );
    }

    @Override
    @Test
    public void findById() {
        testUser = usersService.loadUserByUsername(username);
        Assertions.assertTrue(() -> usersService.findById(testUser.getUserId()).equals(testUser));
    }

    @Override
    @Test
    public void remove() {
        Assertions.assertTrue(
                () -> {
                    int length = users.size();
                    usersService.remove(usersService.loadUserByUsername("test.username"));
                    return getAllUsers().size() == length - 1;
                }
        );
    }
}