package com.example.gamelive.service.impl.base;

import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;

public interface EntityServiceTest<E, ID> {
    void save();
    void update();
    void findById();
    void remove();
}
